import 'dart:developer';

import 'package:flutter/material.dart';

class MyBarra extends StatefulWidget {
  const MyBarra({super.key});

  @override
  State<MyBarra> createState() => _MyBarraState();
}

class _MyBarraState extends State<MyBarra> {
  int paginaActual = 0;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: const [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: "Inicio",
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.phone),
          label: "Telefono",
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.business_sharp),
          label: "Empresa",
        )
      ],
      currentIndex: paginaActual,
      onTap: (value) {
        log(value.toString());
          
        switch (value) {
          case 0:
            Navigator.pushNamed(context, '/casita');
            break;
          case 1:
            Navigator.pushNamed(context, '/telefono');
            break;

          case 2:
            Navigator.pushNamed(context, '/empresa');
            break;
          default:
        }
      },
    );
  }
}
