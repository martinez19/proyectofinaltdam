

import 'package:flutter/material.dart';

class MyFormulario extends StatefulWidget {
  const MyFormulario({super.key});

  @override
  State<MyFormulario> createState() => _MyFormularioState();
}

class _MyFormularioState extends State<MyFormulario> {
  late String valor = "";
  TextEditingController controladorCorreo = TextEditingController();
  TextEditingController controladorTelefono = TextEditingController();
  TextEditingController controladorMensaje = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: TextFormField(
            controller: controladorCorreo,
            decoration: const InputDecoration(
              icon: Icon(Icons.email),
              border: OutlineInputBorder(),
              labelText: "Correo Electronico",
            ),
            keyboardType: TextInputType.emailAddress,
            onChanged: (value) {
              controladorMensaje.value = controladorCorreo.value;
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: TextFormField(
            controller: controladorTelefono,
            decoration: const InputDecoration(
                icon: Icon(Icons.phone),
                border: OutlineInputBorder(),
                labelText: "Telefono"),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: TextFormField(
            controller: controladorMensaje,
            decoration: const InputDecoration(
                icon: Icon(Icons.message),
                border: OutlineInputBorder(),
                labelText: "Mensaje"),
            onChanged: (value) {},
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 170, vertical: 0),
          child: ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/vistados');
            },
            style: const ButtonStyle(
                backgroundColor:
                    MaterialStatePropertyAll<Color>(Colors.yellowAccent),
                fixedSize: MaterialStatePropertyAll<Size>(Size(0, 40))),
            child: const Text(
              "Enviar",
              style: TextStyle(color: Colors.black),
            ),
          ),
        ),
      ],
    );
  }
}
