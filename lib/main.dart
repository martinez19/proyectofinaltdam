import 'package:flutter/material.dart';
import 'package:flutter_formularios/inicio.dart';
import 'package:flutter_formularios/vistas_barra/casita.dart';
import 'package:flutter_formularios/vistas_barra/empresa.dart';
import 'package:flutter_formularios/vistas_barra/telefono.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => const MyInicio(), //vista
        '/casita': (context) => const Casita(),
        '/telefono': (context) => const Telefono(),
        '/empresa': (context) => const Empresa(),
      },
      debugShowCheckedModeBanner: false,
    );
  }
}
