import 'package:flutter/material.dart';
import 'package:flutter_formularios/barra_navegacion/barra_pantalla.dart';

class Telefono extends StatelessWidget {
  const Telefono({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Vista Telefono"),
      ),
      bottomNavigationBar: const MyBarra(),
    );
  }
}
