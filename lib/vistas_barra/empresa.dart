import 'package:flutter/material.dart';
import 'package:flutter_formularios/barra_navegacion/barra_pantalla.dart';

class Empresa extends StatelessWidget {
  const Empresa({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Empresa"),
      ),
      bottomNavigationBar: const MyBarra(),
    );
  }
}
