import 'package:flutter/material.dart';
import 'package:flutter_formularios/barra_navegacion/barra_pantalla.dart';

class Casita extends StatelessWidget {
  const Casita({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Vista Casita"),
        backgroundColor: Colors.yellow,
      ),
      body: ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, '/');
          },
          child: const Text("vista Casa")),
      bottomNavigationBar: const MyBarra(),
    );
  }
}
